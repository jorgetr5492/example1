<?php
class Services_model extends MY_Model{
	function __construct(){
		parent::__construct();
    $this->set_table("servicios");
	}

  public function list_id($id){
  			$query = $this->db->select('*')
  			->from($this->_table)
				->where('id',$id)
  			->get();
				$result = $query->row();
				return ($result)?$result:NULL;
  	}

}
