<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		  $this->load->model('services_model', 'services_model');
	}
	public function index()
	{
		$data['flights']=$this->services_model->list_id(1);

		$this->load->view('welcome_message',$data);
	}
}
